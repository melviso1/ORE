#if !defined(ATTRIBUTEVALUE_H_INCLUDED)
#define ATTRIBUTEVALUE_H_INCLUDED


// {user.before.class.AttributeValue.begin}
// {user.before.class.AttributeValue.end}

class AttributeValue
{

	// {user.inside.class.AttributeValue.begin}
	// {user.inside.class.AttributeValue.end}

	public:
	Reference*  GetFromReference();
	AttributeValue* _pNextFromReference ;
	AttributeValue* _pPrevFromReference ;
	Reference* _pFromReference ;
	AttributeDef*  GetFromAttributeDef();
	AttributeValue* _pNextFromAttributeDef ;
	AttributeValue* _pPrevFromAttributeDef ;
	AttributeDef* _pFromAttributeDef ;
	void  __exit__();
	void  __init__(Reference*  pFromReference, AttributeDef*  pFromAttributeDef);
	virtual ~AttributeValue();
};

// {user.after.class.AttributeValue.begin}
// {user.after.class.AttributeValue.end}

#endif //ATTRIBUTEVALUE_H_INCLUDED


