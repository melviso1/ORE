#if !defined(RELATIONCHAIN_H_INCLUDED)
#define RELATIONCHAIN_H_INCLUDED


// {user.before.class.RelationChain.begin}
#include <list>
#include <algorithm>
// {user.before.class.RelationChain.end}

/**
* Representa a una cadena de relaciones
**/
class RelationChain
{

	// {user.inside.class.RelationChain.begin}
	// {user.inside.class.RelationChain.end}

	friend class ReferenceType;
	public:
	ReferenceType*  GetFromReferenceType();
	RelationChain* _pNextFromReferenceType ;
	RelationChain* _pPrevFromReferenceType ;
	ReferenceType* _pFromReferenceType ;
	ReferenceType*  GetToReferenceType();
	RelationChain* _pNextToReferenceType ;
	RelationChain* _pPrevToReferenceType ;
	ReferenceType* _pToReferenceType ;
	virtual ~RelationChain();
	void  Populate();
	void  __exit__();
	protected:
	RelationChain(ExplicitRelation*  pExplicitRelation, RelationChain*  prev=nullptr);
	public:
	void  __init__(ReferenceType*  pFromReferenceType, ReferenceType*  pToReferenceType);
	private:
	std::list<ExplicitRelation*> _relations ;
};

// {user.after.class.RelationChain.begin}
// {user.after.class.RelationChain.end}

#endif //RELATIONCHAIN_H_INCLUDED


