#if !defined(EXPLICITRELATION_H_INCLUDED)
#define EXPLICITRELATION_H_INCLUDED


// {user.before.class.ExplicitRelation.begin}
// {user.before.class.ExplicitRelation.end}

class ExplicitRelation: public AttributeDef
{

	// {user.inside.class.ExplicitRelation.begin}
	// {user.inside.class.ExplicitRelation.end}

	public:
	ReferenceType*  GetToReferenceType();
	ExplicitRelation* _pNextToReferenceType ;
	ExplicitRelation* _pPrevToReferenceType ;
	ReferenceType* _pToReferenceType ;
	virtual ~ExplicitRelation();
	void  __exit__();
	ExplicitRelation(ReferenceType*  pFromReferenceType, ReferenceType*  pToReferenceType);
	void  __init__(ReferenceType*  pToReferenceType);
};

// {user.after.class.ExplicitRelation.begin}
// {user.after.class.ExplicitRelation.end}

#endif //EXPLICITRELATION_H_INCLUDED


