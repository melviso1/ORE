#if !defined(ATTRIBUTEDEF_H_INCLUDED)
#define ATTRIBUTEDEF_H_INCLUDED


// {user.before.class.AttributeDef.begin}
// {user.before.class.AttributeDef.end}

class AttributeDef
{

	// {user.inside.class.AttributeDef.begin}
	// {user.inside.class.AttributeDef.end}

	friend class AttributeValue;
	public:
	ReferenceType*  GetFromReferenceType();
	AttributeDef* _pNextFromReferenceType ;
	AttributeDef* _pPrevFromReferenceType ;
	ReferenceType* _pFromReferenceType ;
	virtual ~AttributeDef();
	protected:
	void  AddToAttributeValueFirst(AttributeValue*  pToAttributeValue);
	void  AddToAttributeValueLast(AttributeValue*  pToAttributeValue);
	void  AddToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  AddToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  RemoveToAttributeValue(AttributeValue*  pToAttributeValue);
	void  ReplaceToAttributeValue(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValueNew);
	public:
	void  DeleteAllToAttributeValue();
	AttributeValue*  GetFirstToAttributeValue() const;
	AttributeValue*  GetLastToAttributeValue() const;
	AttributeValue*  GetNextToAttributeValue(AttributeValue*  pToAttributeValue) const;
	AttributeValue*  GetPrevToAttributeValue(AttributeValue*  pToAttributeValue) const;
	unsigned long  GetToAttributeValueCount() const;
	void  MoveToAttributeValueFirst(AttributeValue*  pToAttributeValue);
	void  MoveToAttributeValueLast(AttributeValue*  pToAttributeValue);
	void  MoveToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  MoveToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  SortToAttributeValue(int (* compare)(AttributeValue*,AttributeValue*));
	AttributeValue* _pFirstToAttributeValue ;
	AttributeValue* _pLastToAttributeValue ;
	unsigned long _ToAttributeValueCount ;
	void  __exit__();
	AttributeDef(ReferenceType*  pFromReferenceType);
	void  __init__(ReferenceType*  pFromReferenceType);
	const bool  is_ExplicitRelation() const;
};

// {user.after.class.AttributeDef.begin}
// {user.after.class.AttributeDef.end}

#endif //ATTRIBUTEDEF_H_INCLUDED


