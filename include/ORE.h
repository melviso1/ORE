/***

 File    :ORE.h
 Created :27/02/2016 23:09:55
 Author  :Mel Viso

 Licensed under wxWidgets license

***/

#if !defined(ORE_H_INCLUDED)
#define ORE_H_INCLUDED

//forward references
class ReferenceType;
class Reference;
class AttributeDef;
class RelationChain;
class AttributeValue;
class ExplicitRelation;

#include "ReferenceType.h"
#include "Reference.h"
#include "AttributeDef.h"
#include "RelationChain.h"
#include "AttributeValue.h"
#include "ExplicitRelation.h"

#define INCLUDE_INLINES
#include "ReferenceType.h"
#include "Reference.h"
#include "AttributeDef.h"
#include "RelationChain.h"
#include "AttributeValue.h"
#include "ExplicitRelation.h"


#endif //ORE_H_INCLUDED

