#if !defined(REFERENCE_H_INCLUDED)
#define REFERENCE_H_INCLUDED


// {user.before.class.Reference.begin}
#include <string>
// {user.before.class.Reference.end}

class Reference
{

	// {user.inside.class.Reference.begin}
	// {user.inside.class.Reference.end}

	friend class ReferenceType;
	friend class AttributeValue;
	public:
	ReferenceType*  GetFromReferenceType();
	Reference* _pNextFromReferenceType ;
	Reference* _pPrevFromReferenceType ;
	ReferenceType* _pFromReferenceType ;
	virtual ~Reference();
	const std::string&  key();
	protected:
	void  AddToAttributeValueFirst(AttributeValue*  pToAttributeValue);
	void  AddToAttributeValueLast(AttributeValue*  pToAttributeValue);
	void  AddToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  AddToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  RemoveToAttributeValue(AttributeValue*  pToAttributeValue);
	void  ReplaceToAttributeValue(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValueNew);
	public:
	void  DeleteAllToAttributeValue();
	AttributeValue*  GetFirstToAttributeValue() const;
	AttributeValue*  GetLastToAttributeValue() const;
	AttributeValue*  GetNextToAttributeValue(AttributeValue*  pToAttributeValue) const;
	AttributeValue*  GetPrevToAttributeValue(AttributeValue*  pToAttributeValue) const;
	unsigned long  GetToAttributeValueCount() const;
	void  MoveToAttributeValueFirst(AttributeValue*  pToAttributeValue);
	void  MoveToAttributeValueLast(AttributeValue*  pToAttributeValue);
	void  MoveToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  MoveToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos);
	void  SortToAttributeValue(int (* compare)(AttributeValue*,AttributeValue*));
	AttributeValue* _pFirstToAttributeValue ;
	AttributeValue* _pLastToAttributeValue ;
	unsigned long _ToAttributeValueCount ;
	void  __exit__();
	Reference(std::string  key, ReferenceType*  pFromReferenceType);
	void  __init__(ReferenceType*  pFromReferenceType);
	private:
	std::string _key ;
};

// {user.after.class.Reference.begin}
// {user.after.class.Reference.end}

#endif //REFERENCE_H_INCLUDED


