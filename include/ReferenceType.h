#if !defined(REFERENCETYPE_H_INCLUDED)
#define REFERENCETYPE_H_INCLUDED


// {user.before.class.ReferenceType.begin}
#include <string>
// {user.before.class.ReferenceType.end}

class ReferenceType
{

	// {user.inside.class.ReferenceType.begin}
	// {user.inside.class.ReferenceType.end}

	friend class Reference;
	friend class AttributeDef;
	friend class ExplicitRelation;
	friend class RelationChain;
	public:
	virtual ~ReferenceType();
	inline const std::string&  name();
	void  CreateToRelationChains();
	protected:
	void  AddToRelationChainFirst(RelationChain*  pToRelationChain);
	void  AddToRelationChainLast(RelationChain*  pToRelationChain);
	void  AddToRelationChainAfter(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos);
	void  AddToRelationChainBefore(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos);
	void  RemoveToRelationChain(RelationChain*  pToRelationChain);
	void  ReplaceToRelationChain(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainNew);
	public:
	void  DeleteAllToRelationChain();
	RelationChain*  GetFirstToRelationChain() const;
	RelationChain*  GetLastToRelationChain() const;
	RelationChain*  GetNextToRelationChain(RelationChain*  pToRelationChain) const;
	RelationChain*  GetPrevToRelationChain(RelationChain*  pToRelationChain) const;
	unsigned long  GetToRelationChainCount() const;
	void  MoveToRelationChainFirst(RelationChain*  pToRelationChain);
	void  MoveToRelationChainLast(RelationChain*  pToRelationChain);
	void  MoveToRelationChainAfter(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos);
	void  MoveToRelationChainBefore(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos);
	void  SortToRelationChain(int (* compare)(RelationChain*,RelationChain*));
	RelationChain* _pFirstToRelationChain ;
	RelationChain* _pLastToRelationChain ;
	unsigned long _ToRelationChainCount ;
	protected:
	void  AddToReferenceFirst(Reference*  pToReference);
	void  AddToReferenceLast(Reference*  pToReference);
	void  AddToReferenceAfter(Reference*  pToReference, Reference*  pToReferencePos);
	void  AddToReferenceBefore(Reference*  pToReference, Reference*  pToReferencePos);
	void  RemoveToReference(Reference*  pToReference);
	void  ReplaceToReference(Reference*  pToReference, Reference*  pToReferenceNew);
	public:
	void  DeleteAllToReference();
	Reference*  GetFirstToReference() const;
	Reference*  GetLastToReference() const;
	Reference*  GetNextToReference(Reference*  pToReference) const;
	Reference*  GetPrevToReference(Reference*  pToReference) const;
	unsigned long  GetToReferenceCount() const;
	void  MoveToReferenceFirst(Reference*  pToReference);
	void  MoveToReferenceLast(Reference*  pToReference);
	void  MoveToReferenceAfter(Reference*  pToReference, Reference*  pToReferencePos);
	void  MoveToReferenceBefore(Reference*  pToReference, Reference*  pToReferencePos);
	void  SortToReference(int (* compare)(Reference*,Reference*));
	Reference* _pFirstToReference ;
	Reference* _pLastToReference ;
	unsigned long _ToReferenceCount ;
	protected:
	void  AddToAttributeDefFirst(AttributeDef*  pToAttributeDef);
	void  AddToAttributeDefLast(AttributeDef*  pToAttributeDef);
	void  AddToAttributeDefAfter(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos);
	void  AddToAttributeDefBefore(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos);
	void  RemoveToAttributeDef(AttributeDef*  pToAttributeDef);
	void  ReplaceToAttributeDef(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefNew);
	public:
	void  DeleteAllToAttributeDef();
	AttributeDef*  GetFirstToAttributeDef() const;
	AttributeDef*  GetLastToAttributeDef() const;
	AttributeDef*  GetNextToAttributeDef(AttributeDef*  pToAttributeDef) const;
	AttributeDef*  GetPrevToAttributeDef(AttributeDef*  pToAttributeDef) const;
	unsigned long  GetToAttributeDefCount() const;
	void  MoveToAttributeDefFirst(AttributeDef*  pToAttributeDef);
	void  MoveToAttributeDefLast(AttributeDef*  pToAttributeDef);
	void  MoveToAttributeDefAfter(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos);
	void  MoveToAttributeDefBefore(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos);
	void  SortToAttributeDef(int (* compare)(AttributeDef*,AttributeDef*));
	AttributeDef* _pFirstToAttributeDef ;
	AttributeDef* _pLastToAttributeDef ;
	unsigned long _ToAttributeDefCount ;
	protected:
	void  AddFromExplicitRelationFirst(ExplicitRelation*  pFromExplicitRelation);
	void  AddFromExplicitRelationLast(ExplicitRelation*  pFromExplicitRelation);
	void  AddFromExplicitRelationAfter(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos);
	void  AddFromExplicitRelationBefore(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos);
	void  RemoveFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation);
	void  ReplaceFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationNew);
	public:
	void  DeleteAllFromExplicitRelation();
	ExplicitRelation*  GetFirstFromExplicitRelation() const;
	ExplicitRelation*  GetLastFromExplicitRelation() const;
	ExplicitRelation*  GetNextFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation) const;
	ExplicitRelation*  GetPrevFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation) const;
	unsigned long  GetFromExplicitRelationCount() const;
	void  MoveFromExplicitRelationFirst(ExplicitRelation*  pFromExplicitRelation);
	void  MoveFromExplicitRelationLast(ExplicitRelation*  pFromExplicitRelation);
	void  MoveFromExplicitRelationAfter(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos);
	void  MoveFromExplicitRelationBefore(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos);
	void  SortFromExplicitRelation(int (* compare)(ExplicitRelation*,ExplicitRelation*));
	ExplicitRelation* _pFirstFromExplicitRelation ;
	ExplicitRelation* _pLastFromExplicitRelation ;
	unsigned long _FromExplicitRelationCount ;
	protected:
	void  AddFomRelationChainFirst(RelationChain*  pFomRelationChain);
	void  AddFomRelationChainLast(RelationChain*  pFomRelationChain);
	void  AddFomRelationChainAfter(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos);
	void  AddFomRelationChainBefore(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos);
	void  RemoveFomRelationChain(RelationChain*  pFomRelationChain);
	void  ReplaceFomRelationChain(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainNew);
	public:
	void  DeleteAllFomRelationChain();
	RelationChain*  GetFirstFomRelationChain() const;
	RelationChain*  GetLastFomRelationChain() const;
	RelationChain*  GetNextFomRelationChain(RelationChain*  pFomRelationChain) const;
	RelationChain*  GetPrevFomRelationChain(RelationChain*  pFomRelationChain) const;
	unsigned long  GetFomRelationChainCount() const;
	void  MoveFomRelationChainFirst(RelationChain*  pFomRelationChain);
	void  MoveFomRelationChainLast(RelationChain*  pFomRelationChain);
	void  MoveFomRelationChainAfter(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos);
	void  MoveFomRelationChainBefore(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos);
	void  SortFomRelationChain(int (* compare)(RelationChain*,RelationChain*));
	RelationChain* _pFirstFomRelationChain ;
	RelationChain* _pLastFomRelationChain ;
	unsigned long _FomRelationChainCount ;
	void  __exit__();
	ReferenceType(std::string  name);
	private:
	std::string _name ;
};

// {user.after.class.ReferenceType.begin}
// {user.after.class.ReferenceType.end}

#endif //REFERENCETYPE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_REFERENCETYPE)
#define INCLUDE_INLINES_REFERENCETYPE

const std::string&  ReferenceType::name()
{
	return _name;
}

#endif //(INCLUDE_INLINES_REFERENCETYPE)
#endif //INCLUDE_INLINES


