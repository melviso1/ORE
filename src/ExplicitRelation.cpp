// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.ExplicitRelation.begin}
// {user.before.class.ExplicitRelation.end}

void  ExplicitRelation::__init__(ReferenceType*  pToReferenceType)
{
		pToReferenceType->AddFromExplicitRelationLast(this);
}
void  ExplicitRelation::__exit__()
{
		_pToReferenceType->RemoveFromExplicitRelation(this);
}
ExplicitRelation::ExplicitRelation(ReferenceType*  pFromReferenceType, ReferenceType*  pToReferenceType)
:	AttributeDef(pFromReferenceType)
,	_pNextToReferenceType(nullptr)
,	_pPrevToReferenceType(nullptr)
,	_pToReferenceType(pToReferenceType)
{
		__init__(pToReferenceType);
}
ReferenceType*  ExplicitRelation::GetToReferenceType()
{
	
	return _pToReferenceType;
	      
}
ExplicitRelation::~ExplicitRelation()
{
	__exit__();
}

// {user.after.class.ExplicitRelation.begin}
// {user.after.class.ExplicitRelation.end}


