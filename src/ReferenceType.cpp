// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.ReferenceType.begin}
// {user.before.class.ReferenceType.end}

void  ReferenceType::__exit__()
{
		DeleteAllToRelationChain();
		DeleteAllToReference();
		DeleteAllToAttributeDef();
		DeleteAllFromExplicitRelation();
		DeleteAllFomRelationChain();
}
ReferenceType::ReferenceType(std::string  name)
:	_pFirstToRelationChain(nullptr)
,	_pLastToRelationChain(nullptr)
,	_ToRelationChainCount(0UL)
,	_pFirstToReference(nullptr)
,	_pLastToReference(nullptr)
,	_ToReferenceCount(0UL)
,	_pFirstToAttributeDef(nullptr)
,	_pLastToAttributeDef(nullptr)
,	_ToAttributeDefCount(0UL)
,	_pFirstFromExplicitRelation(nullptr)
,	_pLastFromExplicitRelation(nullptr)
,	_FromExplicitRelationCount(0UL)
,	_pFirstFomRelationChain(nullptr)
,	_pLastFomRelationChain(nullptr)
,	_FomRelationChainCount(0UL)
,	_name(name)
{
}
/**
* Generates all the relation chains from this reference type
**/
void  ReferenceType::CreateToRelationChains()
{
	DeleteAllToRelationChain();
	AttributeDef *pAttributeDef = GetFirstToAttributeDef();
	while(pAttributeDef != nullptr)
	{
		ExplicitRelation *pExplicitRelation = dynamic_cast<ExplicitRelation *>(pAttributeDef);
		if(pExplicitRelation != nullptr)
		{
			new RelationChain(pExplicitRelation);
		}
	}
}
void  ReferenceType::AddToRelationChainFirst(RelationChain*  pToRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain);
		#if !defined(TOLERANT)
			assert( pToRelationChain->_pFromReferenceType == nullptr );
		#else
			if ( pToRelationChain->_pFromReferenceType != nullptr )
			{
				assert( pToRelationChain->_pFromReferenceType == this );
				MoveToRelationChainFirst( pToRelationChain );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToRelationChainCount++;
	pToRelationChain->_pFromReferenceType = this;
	if ( _pFirstToRelationChain != nullptr )
	{
		pToRelationChain->_pNextFromReferenceType = _pFirstToRelationChain;
		_pFirstToRelationChain->_pPrevFromReferenceType = pToRelationChain;
		_pFirstToRelationChain = pToRelationChain;
	}
	else
	{
		_pFirstToRelationChain = pToRelationChain;
		_pLastToRelationChain = pToRelationChain;
	}
	      
}
void  ReferenceType::AddToRelationChainLast(RelationChain*  pToRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain);
		#if !defined(TOLERANT)
			assert(pToRelationChain->_pFromReferenceType == nullptr);
		#else
			if(pToRelationChain->_pFromReferenceType != nullptr)
			{
				assert(pToRelationChain->_pFromReferenceType == this);
				MoveToRelationChainLast(pToRelationChain);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToRelationChainCount++;
	pToRelationChain->_pFromReferenceType = this;
	if (_pLastToRelationChain != nullptr)
	{
		pToRelationChain->_pPrevFromReferenceType = _pLastToRelationChain;
		_pLastToRelationChain->_pNextFromReferenceType = pToRelationChain;
		_pLastToRelationChain = pToRelationChain;
	}
	else
	{
		_pLastToRelationChain = pToRelationChain;
		_pFirstToRelationChain = pToRelationChain;
	}
	      
}
void  ReferenceType::AddToRelationChainAfter(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain);
		assert(pToRelationChainPos);
		assert(pToRelationChainPos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToRelationChain->_pFromReferenceType == nullptr);
		#else
			if(pToRelationChain->_pFromReferenceType != nullptr)
			{
				assert(pToRelationChain->_pFromReferenceType == this);
				MoveToRelationChainAfter(pToRelationChain,pToRelationChainPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToRelationChainCount++;
	pToRelationChain->_pFromReferenceType = this;
	pToRelationChain->_pPrevFromReferenceType = pToRelationChainPos;
	pToRelationChain->_pNextFromReferenceType = pToRelationChainPos->_pNextFromReferenceType;
	pToRelationChainPos->_pNextFromReferenceType  = pToRelationChain;
	if (pToRelationChain->_pNextFromReferenceType != nullptr)
	{
		pToRelationChain->_pNextFromReferenceType->_pPrevFromReferenceType = pToRelationChain;
	}
	else
	{
		_pLastToRelationChain = pToRelationChain;
	}
	      
}
void  ReferenceType::AddToRelationChainBefore(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain);
		assert(pToRelationChainPos);
		assert(pToRelationChainPos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToRelationChain->_pFromReferenceType == nullptr);
		#else
			if (pToRelationChain->_pFromReferenceType != nullptr)
			{
				assert(pToRelationChain->_pFromReferenceType == this);
				MoveToRelationChainBefore(pToRelationChain,pToRelationChainPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToRelationChainCount++;
	pToRelationChain->_pFromReferenceType = this;
	pToRelationChain->_pNextFromReferenceType = pToRelationChainPos;
	pToRelationChain->_pPrevFromReferenceType = pToRelationChainPos->_pPrevFromReferenceType;
	pToRelationChainPos->_pPrevFromReferenceType = pToRelationChain;
	if ( pToRelationChain->_pPrevFromReferenceType != nullptr )
	{
		pToRelationChain->_pPrevFromReferenceType->_pNextFromReferenceType = pToRelationChain;
	}
	else
	{
		_pFirstToRelationChain = pToRelationChain;
	}
			
}
void  ReferenceType::RemoveToRelationChain(RelationChain*  pToRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain);
		#if !defined(TOLERANT)
			assert( pToRelationChain->_pFromReferenceType == this );
		#else
			if( pToRelationChain->_pFromReferenceType != this )
			{
				assert( pToRelationChain->_pFromReferenceType == nullptr );
				if( pToRelationChain->_pPrevFromReferenceType != nullptr )
				{
					assert( pToRelationChain->_pPrevFromReferenceType->_pNextFromReferenceType != pToRelationChain );
				}
				if( pToRelationChain->_pNextFromReferenceType != nullptr )
				{
					assert( pToRelationChain->_pNextFromReferenceType->_pPrevFromReferenceType != pToRelationChain );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToRelationChainCount++;
	if ( pToRelationChain->_pNextFromReferenceType != nullptr )
	{
		pToRelationChain->_pNextFromReferenceType->_pPrevFromReferenceType = pToRelationChain->_pPrevFromReferenceType;
	}
	else
	{
		_pLastToRelationChain = pToRelationChain->_pPrevFromReferenceType;
	}
	if ( pToRelationChain->_pPrevFromReferenceType != nullptr )
	{
		pToRelationChain->_pPrevFromReferenceType->_pNextFromReferenceType = pToRelationChain->_pNextFromReferenceType;
	}
	else
	{
		_pFirstToRelationChain = pToRelationChain->_pNextFromReferenceType;
	}
	pToRelationChain->_pPrevFromReferenceType = nullptr;
	pToRelationChain->_pNextFromReferenceType = nullptr;
	pToRelationChain->_pFromReferenceType = nullptr;
			
}
void  ReferenceType::ReplaceToRelationChain(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainNew)
{
	
	assert(this);
	assert(pToRelationChain->_pFromReferenceType == this);
	assert(pToRelationChainNew->_pFromReferenceType == nullptr);
	if (pToRelationChain->_pNextFromReferenceType != nullptr)
	{
		pToRelationChain->_pNextFromReferenceType->_pPrevFromReferenceType = pToRelationChainNew;
	}
	else
	{
		_pLastToRelationChain = pToRelationChainNew;
	}
	if (pToRelationChain->_pPrevFromReferenceType != nullptr)
	{
		pToRelationChain->_pPrevFromReferenceType->_pNextFromReferenceType = pToRelationChainNew;
	}
	else
	{
		_pFirstToRelationChain = pToRelationChainNew;
	}
	pToRelationChainNew->_pNextFromReferenceType = pToRelationChain->_pNextFromReferenceType;
	pToRelationChainNew->_pPrevFromReferenceType = pToRelationChain->_pPrevFromReferenceType;
	pToRelationChain->_pNextFromReferenceType = nullptr;
	pToRelationChain->_pPrevFromReferenceType = nullptr;
	pToRelationChain->_pFromReferenceType = nullptr;
	pToRelationChainNew->_pFromReferenceType = this;
			
}
void  ReferenceType::DeleteAllToRelationChain()
{
	 
	RelationChain *elem = GetFirstToRelationChain();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstToRelationChain();
	}
	      
}
RelationChain*  ReferenceType::GetFirstToRelationChain() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstToRelationChain;
	    
}
RelationChain*  ReferenceType::GetLastToRelationChain() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastToRelationChain;
}
RelationChain*  ReferenceType::GetNextToRelationChain(RelationChain*  pToRelationChain) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToRelationChain->_pFromReferenceType == this);
	#endif
	return pToRelationChain->_pNextFromReferenceType; 
	      
}
RelationChain*  ReferenceType::GetPrevToRelationChain(RelationChain*  pToRelationChain) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pToRelationChain == nullptr )
	{
		return _pLastToRelationChain;
	}
	assert(pToRelationChain->_pFromReferenceType == this);
	return pToRelationChain->_pPrevFromReferenceType;
	      
}
unsigned long  ReferenceType::GetToRelationChainCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _ToRelationChainCount;
	      
}
void  ReferenceType::MoveToRelationChainFirst(RelationChain*  pToRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToRelationChain);
		assert(pToRelationChain->_pFromReferenceType);
	#endif
	pToRelationChain->_pFromReferenceType->RemoveToRelationChain(pToRelationChain);
	AddToRelationChainFirst(pToRelationChain); 
	      
}
void  ReferenceType::MoveToRelationChainLast(RelationChain*  pToRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToRelationChain);
		assert(pToRelationChain->_pFromReferenceType);
	#endif
	pToRelationChain->_pFromReferenceType->RemoveToRelationChain(pToRelationChain);
	AddToRelationChainLast(pToRelationChain); 
	      
}
void  ReferenceType::MoveToRelationChainAfter(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToRelationChain);
		assert(pToRelationChain->_pFromReferenceType);
	#endif
	pToRelationChain->_pFromReferenceType->RemoveToRelationChain(pToRelationChain);
	AddToRelationChainAfter(pToRelationChain,pToRelationChainPos); 
}
void  ReferenceType::MoveToRelationChainBefore(RelationChain*  pToRelationChain, RelationChain*  pToRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToRelationChain);
		assert(pToRelationChain->_pFromReferenceType);
	#endif
	pToRelationChain->_pFromReferenceType->RemoveToRelationChain(pToRelationChain);
	AddToRelationChainBefore(pToRelationChain,pToRelationChainPos); 
}
void  ReferenceType::SortToRelationChain(int (* compare)(RelationChain*,RelationChain*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstToRelationChain(); ptr != nullptr; ptr = GetNextToRelationChain(ptr))
	{
		auto pNext = GetNextToRelationChain(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevToRelationChain(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevToRelationChain(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveToRelationChainAfter(pNext, pPrev);
			}
			else
			{
				MoveToRelationChainFirst(pNext);
			}
			pNext = GetNextToRelationChain(ptr);
		}
	}
	      
}
void  ReferenceType::AddToReferenceFirst(Reference*  pToReference)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference);
		#if !defined(TOLERANT)
			assert( pToReference->_pFromReferenceType == nullptr );
		#else
			if ( pToReference->_pFromReferenceType != nullptr )
			{
				assert( pToReference->_pFromReferenceType == this );
				MoveToReferenceFirst( pToReference );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToReferenceCount++;
	pToReference->_pFromReferenceType = this;
	if ( _pFirstToReference != nullptr )
	{
		pToReference->_pNextFromReferenceType = _pFirstToReference;
		_pFirstToReference->_pPrevFromReferenceType = pToReference;
		_pFirstToReference = pToReference;
	}
	else
	{
		_pFirstToReference = pToReference;
		_pLastToReference = pToReference;
	}
	      
}
void  ReferenceType::AddToReferenceLast(Reference*  pToReference)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference);
		#if !defined(TOLERANT)
			assert(pToReference->_pFromReferenceType == nullptr);
		#else
			if(pToReference->_pFromReferenceType != nullptr)
			{
				assert(pToReference->_pFromReferenceType == this);
				MoveToReferenceLast(pToReference);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToReferenceCount++;
	pToReference->_pFromReferenceType = this;
	if (_pLastToReference != nullptr)
	{
		pToReference->_pPrevFromReferenceType = _pLastToReference;
		_pLastToReference->_pNextFromReferenceType = pToReference;
		_pLastToReference = pToReference;
	}
	else
	{
		_pLastToReference = pToReference;
		_pFirstToReference = pToReference;
	}
	      
}
void  ReferenceType::AddToReferenceAfter(Reference*  pToReference, Reference*  pToReferencePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference);
		assert(pToReferencePos);
		assert(pToReferencePos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToReference->_pFromReferenceType == nullptr);
		#else
			if(pToReference->_pFromReferenceType != nullptr)
			{
				assert(pToReference->_pFromReferenceType == this);
				MoveToReferenceAfter(pToReference,pToReferencePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToReferenceCount++;
	pToReference->_pFromReferenceType = this;
	pToReference->_pPrevFromReferenceType = pToReferencePos;
	pToReference->_pNextFromReferenceType = pToReferencePos->_pNextFromReferenceType;
	pToReferencePos->_pNextFromReferenceType  = pToReference;
	if (pToReference->_pNextFromReferenceType != nullptr)
	{
		pToReference->_pNextFromReferenceType->_pPrevFromReferenceType = pToReference;
	}
	else
	{
		_pLastToReference = pToReference;
	}
	      
}
void  ReferenceType::AddToReferenceBefore(Reference*  pToReference, Reference*  pToReferencePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference);
		assert(pToReferencePos);
		assert(pToReferencePos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToReference->_pFromReferenceType == nullptr);
		#else
			if (pToReference->_pFromReferenceType != nullptr)
			{
				assert(pToReference->_pFromReferenceType == this);
				MoveToReferenceBefore(pToReference,pToReferencePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToReferenceCount++;
	pToReference->_pFromReferenceType = this;
	pToReference->_pNextFromReferenceType = pToReferencePos;
	pToReference->_pPrevFromReferenceType = pToReferencePos->_pPrevFromReferenceType;
	pToReferencePos->_pPrevFromReferenceType = pToReference;
	if ( pToReference->_pPrevFromReferenceType != nullptr )
	{
		pToReference->_pPrevFromReferenceType->_pNextFromReferenceType = pToReference;
	}
	else
	{
		_pFirstToReference = pToReference;
	}
			
}
void  ReferenceType::RemoveToReference(Reference*  pToReference)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference);
		#if !defined(TOLERANT)
			assert( pToReference->_pFromReferenceType == this );
		#else
			if( pToReference->_pFromReferenceType != this )
			{
				assert( pToReference->_pFromReferenceType == nullptr );
				if( pToReference->_pPrevFromReferenceType != nullptr )
				{
					assert( pToReference->_pPrevFromReferenceType->_pNextFromReferenceType != pToReference );
				}
				if( pToReference->_pNextFromReferenceType != nullptr )
				{
					assert( pToReference->_pNextFromReferenceType->_pPrevFromReferenceType != pToReference );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToReferenceCount++;
	if ( pToReference->_pNextFromReferenceType != nullptr )
	{
		pToReference->_pNextFromReferenceType->_pPrevFromReferenceType = pToReference->_pPrevFromReferenceType;
	}
	else
	{
		_pLastToReference = pToReference->_pPrevFromReferenceType;
	}
	if ( pToReference->_pPrevFromReferenceType != nullptr )
	{
		pToReference->_pPrevFromReferenceType->_pNextFromReferenceType = pToReference->_pNextFromReferenceType;
	}
	else
	{
		_pFirstToReference = pToReference->_pNextFromReferenceType;
	}
	pToReference->_pPrevFromReferenceType = nullptr;
	pToReference->_pNextFromReferenceType = nullptr;
	pToReference->_pFromReferenceType = nullptr;
			
}
void  ReferenceType::ReplaceToReference(Reference*  pToReference, Reference*  pToReferenceNew)
{
	
	assert(this);
	assert(pToReference->_pFromReferenceType == this);
	assert(pToReferenceNew->_pFromReferenceType == nullptr);
	if (pToReference->_pNextFromReferenceType != nullptr)
	{
		pToReference->_pNextFromReferenceType->_pPrevFromReferenceType = pToReferenceNew;
	}
	else
	{
		_pLastToReference = pToReferenceNew;
	}
	if (pToReference->_pPrevFromReferenceType != nullptr)
	{
		pToReference->_pPrevFromReferenceType->_pNextFromReferenceType = pToReferenceNew;
	}
	else
	{
		_pFirstToReference = pToReferenceNew;
	}
	pToReferenceNew->_pNextFromReferenceType = pToReference->_pNextFromReferenceType;
	pToReferenceNew->_pPrevFromReferenceType = pToReference->_pPrevFromReferenceType;
	pToReference->_pNextFromReferenceType = nullptr;
	pToReference->_pPrevFromReferenceType = nullptr;
	pToReference->_pFromReferenceType = nullptr;
	pToReferenceNew->_pFromReferenceType = this;
			
}
void  ReferenceType::DeleteAllToReference()
{
	 
	Reference *elem = GetFirstToReference();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstToReference();
	}
	      
}
Reference*  ReferenceType::GetFirstToReference() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstToReference;
	    
}
Reference*  ReferenceType::GetLastToReference() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastToReference;
}
Reference*  ReferenceType::GetNextToReference(Reference*  pToReference) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToReference->_pFromReferenceType == this);
	#endif
	return pToReference->_pNextFromReferenceType; 
	      
}
Reference*  ReferenceType::GetPrevToReference(Reference*  pToReference) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pToReference == nullptr )
	{
		return _pLastToReference;
	}
	assert(pToReference->_pFromReferenceType == this);
	return pToReference->_pPrevFromReferenceType;
	      
}
unsigned long  ReferenceType::GetToReferenceCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _ToReferenceCount;
	      
}
void  ReferenceType::MoveToReferenceFirst(Reference*  pToReference)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToReference);
		assert(pToReference->_pFromReferenceType);
	#endif
	pToReference->_pFromReferenceType->RemoveToReference(pToReference);
	AddToReferenceFirst(pToReference); 
	      
}
void  ReferenceType::MoveToReferenceLast(Reference*  pToReference)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToReference);
		assert(pToReference->_pFromReferenceType);
	#endif
	pToReference->_pFromReferenceType->RemoveToReference(pToReference);
	AddToReferenceLast(pToReference); 
	      
}
void  ReferenceType::MoveToReferenceAfter(Reference*  pToReference, Reference*  pToReferencePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToReference);
		assert(pToReference->_pFromReferenceType);
	#endif
	pToReference->_pFromReferenceType->RemoveToReference(pToReference);
	AddToReferenceAfter(pToReference,pToReferencePos); 
}
void  ReferenceType::MoveToReferenceBefore(Reference*  pToReference, Reference*  pToReferencePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToReference);
		assert(pToReference->_pFromReferenceType);
	#endif
	pToReference->_pFromReferenceType->RemoveToReference(pToReference);
	AddToReferenceBefore(pToReference,pToReferencePos); 
}
void  ReferenceType::SortToReference(int (* compare)(Reference*,Reference*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstToReference(); ptr != nullptr; ptr = GetNextToReference(ptr))
	{
		auto pNext = GetNextToReference(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevToReference(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevToReference(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveToReferenceAfter(pNext, pPrev);
			}
			else
			{
				MoveToReferenceFirst(pNext);
			}
			pNext = GetNextToReference(ptr);
		}
	}
	      
}
void  ReferenceType::AddToAttributeDefFirst(AttributeDef*  pToAttributeDef)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef);
		#if !defined(TOLERANT)
			assert( pToAttributeDef->_pFromReferenceType == nullptr );
		#else
			if ( pToAttributeDef->_pFromReferenceType != nullptr )
			{
				assert( pToAttributeDef->_pFromReferenceType == this );
				MoveToAttributeDefFirst( pToAttributeDef );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeDefCount++;
	pToAttributeDef->_pFromReferenceType = this;
	if ( _pFirstToAttributeDef != nullptr )
	{
		pToAttributeDef->_pNextFromReferenceType = _pFirstToAttributeDef;
		_pFirstToAttributeDef->_pPrevFromReferenceType = pToAttributeDef;
		_pFirstToAttributeDef = pToAttributeDef;
	}
	else
	{
		_pFirstToAttributeDef = pToAttributeDef;
		_pLastToAttributeDef = pToAttributeDef;
	}
	      
}
void  ReferenceType::AddToAttributeDefLast(AttributeDef*  pToAttributeDef)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef);
		#if !defined(TOLERANT)
			assert(pToAttributeDef->_pFromReferenceType == nullptr);
		#else
			if(pToAttributeDef->_pFromReferenceType != nullptr)
			{
				assert(pToAttributeDef->_pFromReferenceType == this);
				MoveToAttributeDefLast(pToAttributeDef);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeDefCount++;
	pToAttributeDef->_pFromReferenceType = this;
	if (_pLastToAttributeDef != nullptr)
	{
		pToAttributeDef->_pPrevFromReferenceType = _pLastToAttributeDef;
		_pLastToAttributeDef->_pNextFromReferenceType = pToAttributeDef;
		_pLastToAttributeDef = pToAttributeDef;
	}
	else
	{
		_pLastToAttributeDef = pToAttributeDef;
		_pFirstToAttributeDef = pToAttributeDef;
	}
	      
}
void  ReferenceType::AddToAttributeDefAfter(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef);
		assert(pToAttributeDefPos);
		assert(pToAttributeDefPos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToAttributeDef->_pFromReferenceType == nullptr);
		#else
			if(pToAttributeDef->_pFromReferenceType != nullptr)
			{
				assert(pToAttributeDef->_pFromReferenceType == this);
				MoveToAttributeDefAfter(pToAttributeDef,pToAttributeDefPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeDefCount++;
	pToAttributeDef->_pFromReferenceType = this;
	pToAttributeDef->_pPrevFromReferenceType = pToAttributeDefPos;
	pToAttributeDef->_pNextFromReferenceType = pToAttributeDefPos->_pNextFromReferenceType;
	pToAttributeDefPos->_pNextFromReferenceType  = pToAttributeDef;
	if (pToAttributeDef->_pNextFromReferenceType != nullptr)
	{
		pToAttributeDef->_pNextFromReferenceType->_pPrevFromReferenceType = pToAttributeDef;
	}
	else
	{
		_pLastToAttributeDef = pToAttributeDef;
	}
	      
}
void  ReferenceType::AddToAttributeDefBefore(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef);
		assert(pToAttributeDefPos);
		assert(pToAttributeDefPos->_pFromReferenceType == this);
		#if !defined(TOLERANT)
			assert(pToAttributeDef->_pFromReferenceType == nullptr);
		#else
			if (pToAttributeDef->_pFromReferenceType != nullptr)
			{
				assert(pToAttributeDef->_pFromReferenceType == this);
				MoveToAttributeDefBefore(pToAttributeDef,pToAttributeDefPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeDefCount++;
	pToAttributeDef->_pFromReferenceType = this;
	pToAttributeDef->_pNextFromReferenceType = pToAttributeDefPos;
	pToAttributeDef->_pPrevFromReferenceType = pToAttributeDefPos->_pPrevFromReferenceType;
	pToAttributeDefPos->_pPrevFromReferenceType = pToAttributeDef;
	if ( pToAttributeDef->_pPrevFromReferenceType != nullptr )
	{
		pToAttributeDef->_pPrevFromReferenceType->_pNextFromReferenceType = pToAttributeDef;
	}
	else
	{
		_pFirstToAttributeDef = pToAttributeDef;
	}
			
}
void  ReferenceType::RemoveToAttributeDef(AttributeDef*  pToAttributeDef)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef);
		#if !defined(TOLERANT)
			assert( pToAttributeDef->_pFromReferenceType == this );
		#else
			if( pToAttributeDef->_pFromReferenceType != this )
			{
				assert( pToAttributeDef->_pFromReferenceType == nullptr );
				if( pToAttributeDef->_pPrevFromReferenceType != nullptr )
				{
					assert( pToAttributeDef->_pPrevFromReferenceType->_pNextFromReferenceType != pToAttributeDef );
				}
				if( pToAttributeDef->_pNextFromReferenceType != nullptr )
				{
					assert( pToAttributeDef->_pNextFromReferenceType->_pPrevFromReferenceType != pToAttributeDef );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeDefCount++;
	if ( pToAttributeDef->_pNextFromReferenceType != nullptr )
	{
		pToAttributeDef->_pNextFromReferenceType->_pPrevFromReferenceType = pToAttributeDef->_pPrevFromReferenceType;
	}
	else
	{
		_pLastToAttributeDef = pToAttributeDef->_pPrevFromReferenceType;
	}
	if ( pToAttributeDef->_pPrevFromReferenceType != nullptr )
	{
		pToAttributeDef->_pPrevFromReferenceType->_pNextFromReferenceType = pToAttributeDef->_pNextFromReferenceType;
	}
	else
	{
		_pFirstToAttributeDef = pToAttributeDef->_pNextFromReferenceType;
	}
	pToAttributeDef->_pPrevFromReferenceType = nullptr;
	pToAttributeDef->_pNextFromReferenceType = nullptr;
	pToAttributeDef->_pFromReferenceType = nullptr;
			
}
void  ReferenceType::ReplaceToAttributeDef(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefNew)
{
	
	assert(this);
	assert(pToAttributeDef->_pFromReferenceType == this);
	assert(pToAttributeDefNew->_pFromReferenceType == nullptr);
	if (pToAttributeDef->_pNextFromReferenceType != nullptr)
	{
		pToAttributeDef->_pNextFromReferenceType->_pPrevFromReferenceType = pToAttributeDefNew;
	}
	else
	{
		_pLastToAttributeDef = pToAttributeDefNew;
	}
	if (pToAttributeDef->_pPrevFromReferenceType != nullptr)
	{
		pToAttributeDef->_pPrevFromReferenceType->_pNextFromReferenceType = pToAttributeDefNew;
	}
	else
	{
		_pFirstToAttributeDef = pToAttributeDefNew;
	}
	pToAttributeDefNew->_pNextFromReferenceType = pToAttributeDef->_pNextFromReferenceType;
	pToAttributeDefNew->_pPrevFromReferenceType = pToAttributeDef->_pPrevFromReferenceType;
	pToAttributeDef->_pNextFromReferenceType = nullptr;
	pToAttributeDef->_pPrevFromReferenceType = nullptr;
	pToAttributeDef->_pFromReferenceType = nullptr;
	pToAttributeDefNew->_pFromReferenceType = this;
			
}
void  ReferenceType::DeleteAllToAttributeDef()
{
	 
	AttributeDef *elem = GetFirstToAttributeDef();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstToAttributeDef();
	}
	      
}
AttributeDef*  ReferenceType::GetFirstToAttributeDef() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstToAttributeDef;
	    
}
AttributeDef*  ReferenceType::GetLastToAttributeDef() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastToAttributeDef;
}
AttributeDef*  ReferenceType::GetNextToAttributeDef(AttributeDef*  pToAttributeDef) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeDef->_pFromReferenceType == this);
	#endif
	return pToAttributeDef->_pNextFromReferenceType; 
	      
}
AttributeDef*  ReferenceType::GetPrevToAttributeDef(AttributeDef*  pToAttributeDef) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pToAttributeDef == nullptr )
	{
		return _pLastToAttributeDef;
	}
	assert(pToAttributeDef->_pFromReferenceType == this);
	return pToAttributeDef->_pPrevFromReferenceType;
	      
}
unsigned long  ReferenceType::GetToAttributeDefCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _ToAttributeDefCount;
	      
}
void  ReferenceType::MoveToAttributeDefFirst(AttributeDef*  pToAttributeDef)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeDef);
		assert(pToAttributeDef->_pFromReferenceType);
	#endif
	pToAttributeDef->_pFromReferenceType->RemoveToAttributeDef(pToAttributeDef);
	AddToAttributeDefFirst(pToAttributeDef); 
	      
}
void  ReferenceType::MoveToAttributeDefLast(AttributeDef*  pToAttributeDef)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeDef);
		assert(pToAttributeDef->_pFromReferenceType);
	#endif
	pToAttributeDef->_pFromReferenceType->RemoveToAttributeDef(pToAttributeDef);
	AddToAttributeDefLast(pToAttributeDef); 
	      
}
void  ReferenceType::MoveToAttributeDefAfter(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeDef);
		assert(pToAttributeDef->_pFromReferenceType);
	#endif
	pToAttributeDef->_pFromReferenceType->RemoveToAttributeDef(pToAttributeDef);
	AddToAttributeDefAfter(pToAttributeDef,pToAttributeDefPos); 
}
void  ReferenceType::MoveToAttributeDefBefore(AttributeDef*  pToAttributeDef, AttributeDef*  pToAttributeDefPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeDef);
		assert(pToAttributeDef->_pFromReferenceType);
	#endif
	pToAttributeDef->_pFromReferenceType->RemoveToAttributeDef(pToAttributeDef);
	AddToAttributeDefBefore(pToAttributeDef,pToAttributeDefPos); 
}
void  ReferenceType::SortToAttributeDef(int (* compare)(AttributeDef*,AttributeDef*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstToAttributeDef(); ptr != nullptr; ptr = GetNextToAttributeDef(ptr))
	{
		auto pNext = GetNextToAttributeDef(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevToAttributeDef(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevToAttributeDef(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveToAttributeDefAfter(pNext, pPrev);
			}
			else
			{
				MoveToAttributeDefFirst(pNext);
			}
			pNext = GetNextToAttributeDef(ptr);
		}
	}
	      
}
void  ReferenceType::AddFromExplicitRelationFirst(ExplicitRelation*  pFromExplicitRelation)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation);
		#if !defined(TOLERANT)
			assert( pFromExplicitRelation->_pToReferenceType == nullptr );
		#else
			if ( pFromExplicitRelation->_pToReferenceType != nullptr )
			{
				assert( pFromExplicitRelation->_pToReferenceType == this );
				MoveFromExplicitRelationFirst( pFromExplicitRelation );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FromExplicitRelationCount++;
	pFromExplicitRelation->_pToReferenceType = this;
	if ( _pFirstFromExplicitRelation != nullptr )
	{
		pFromExplicitRelation->_pNextToReferenceType = _pFirstFromExplicitRelation;
		_pFirstFromExplicitRelation->_pPrevToReferenceType = pFromExplicitRelation;
		_pFirstFromExplicitRelation = pFromExplicitRelation;
	}
	else
	{
		_pFirstFromExplicitRelation = pFromExplicitRelation;
		_pLastFromExplicitRelation = pFromExplicitRelation;
	}
	      
}
void  ReferenceType::AddFromExplicitRelationLast(ExplicitRelation*  pFromExplicitRelation)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation);
		#if !defined(TOLERANT)
			assert(pFromExplicitRelation->_pToReferenceType == nullptr);
		#else
			if(pFromExplicitRelation->_pToReferenceType != nullptr)
			{
				assert(pFromExplicitRelation->_pToReferenceType == this);
				MoveFromExplicitRelationLast(pFromExplicitRelation);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FromExplicitRelationCount++;
	pFromExplicitRelation->_pToReferenceType = this;
	if (_pLastFromExplicitRelation != nullptr)
	{
		pFromExplicitRelation->_pPrevToReferenceType = _pLastFromExplicitRelation;
		_pLastFromExplicitRelation->_pNextToReferenceType = pFromExplicitRelation;
		_pLastFromExplicitRelation = pFromExplicitRelation;
	}
	else
	{
		_pLastFromExplicitRelation = pFromExplicitRelation;
		_pFirstFromExplicitRelation = pFromExplicitRelation;
	}
	      
}
void  ReferenceType::AddFromExplicitRelationAfter(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelationPos);
		assert(pFromExplicitRelationPos->_pToReferenceType == this);
		#if !defined(TOLERANT)
			assert(pFromExplicitRelation->_pToReferenceType == nullptr);
		#else
			if(pFromExplicitRelation->_pToReferenceType != nullptr)
			{
				assert(pFromExplicitRelation->_pToReferenceType == this);
				MoveFromExplicitRelationAfter(pFromExplicitRelation,pFromExplicitRelationPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FromExplicitRelationCount++;
	pFromExplicitRelation->_pToReferenceType = this;
	pFromExplicitRelation->_pPrevToReferenceType = pFromExplicitRelationPos;
	pFromExplicitRelation->_pNextToReferenceType = pFromExplicitRelationPos->_pNextToReferenceType;
	pFromExplicitRelationPos->_pNextToReferenceType  = pFromExplicitRelation;
	if (pFromExplicitRelation->_pNextToReferenceType != nullptr)
	{
		pFromExplicitRelation->_pNextToReferenceType->_pPrevToReferenceType = pFromExplicitRelation;
	}
	else
	{
		_pLastFromExplicitRelation = pFromExplicitRelation;
	}
	      
}
void  ReferenceType::AddFromExplicitRelationBefore(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelationPos);
		assert(pFromExplicitRelationPos->_pToReferenceType == this);
		#if !defined(TOLERANT)
			assert(pFromExplicitRelation->_pToReferenceType == nullptr);
		#else
			if (pFromExplicitRelation->_pToReferenceType != nullptr)
			{
				assert(pFromExplicitRelation->_pToReferenceType == this);
				MoveFromExplicitRelationBefore(pFromExplicitRelation,pFromExplicitRelationPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FromExplicitRelationCount++;
	pFromExplicitRelation->_pToReferenceType = this;
	pFromExplicitRelation->_pNextToReferenceType = pFromExplicitRelationPos;
	pFromExplicitRelation->_pPrevToReferenceType = pFromExplicitRelationPos->_pPrevToReferenceType;
	pFromExplicitRelationPos->_pPrevToReferenceType = pFromExplicitRelation;
	if ( pFromExplicitRelation->_pPrevToReferenceType != nullptr )
	{
		pFromExplicitRelation->_pPrevToReferenceType->_pNextToReferenceType = pFromExplicitRelation;
	}
	else
	{
		_pFirstFromExplicitRelation = pFromExplicitRelation;
	}
			
}
void  ReferenceType::RemoveFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation);
		#if !defined(TOLERANT)
			assert( pFromExplicitRelation->_pToReferenceType == this );
		#else
			if( pFromExplicitRelation->_pToReferenceType != this )
			{
				assert( pFromExplicitRelation->_pToReferenceType == nullptr );
				if( pFromExplicitRelation->_pPrevToReferenceType != nullptr )
				{
					assert( pFromExplicitRelation->_pPrevToReferenceType->_pNextToReferenceType != pFromExplicitRelation );
				}
				if( pFromExplicitRelation->_pNextToReferenceType != nullptr )
				{
					assert( pFromExplicitRelation->_pNextToReferenceType->_pPrevToReferenceType != pFromExplicitRelation );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FromExplicitRelationCount++;
	if ( pFromExplicitRelation->_pNextToReferenceType != nullptr )
	{
		pFromExplicitRelation->_pNextToReferenceType->_pPrevToReferenceType = pFromExplicitRelation->_pPrevToReferenceType;
	}
	else
	{
		_pLastFromExplicitRelation = pFromExplicitRelation->_pPrevToReferenceType;
	}
	if ( pFromExplicitRelation->_pPrevToReferenceType != nullptr )
	{
		pFromExplicitRelation->_pPrevToReferenceType->_pNextToReferenceType = pFromExplicitRelation->_pNextToReferenceType;
	}
	else
	{
		_pFirstFromExplicitRelation = pFromExplicitRelation->_pNextToReferenceType;
	}
	pFromExplicitRelation->_pPrevToReferenceType = nullptr;
	pFromExplicitRelation->_pNextToReferenceType = nullptr;
	pFromExplicitRelation->_pToReferenceType = nullptr;
			
}
void  ReferenceType::ReplaceFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationNew)
{
	
	assert(this);
	assert(pFromExplicitRelation->_pToReferenceType == this);
	assert(pFromExplicitRelationNew->_pToReferenceType == nullptr);
	if (pFromExplicitRelation->_pNextToReferenceType != nullptr)
	{
		pFromExplicitRelation->_pNextToReferenceType->_pPrevToReferenceType = pFromExplicitRelationNew;
	}
	else
	{
		_pLastFromExplicitRelation = pFromExplicitRelationNew;
	}
	if (pFromExplicitRelation->_pPrevToReferenceType != nullptr)
	{
		pFromExplicitRelation->_pPrevToReferenceType->_pNextToReferenceType = pFromExplicitRelationNew;
	}
	else
	{
		_pFirstFromExplicitRelation = pFromExplicitRelationNew;
	}
	pFromExplicitRelationNew->_pNextToReferenceType = pFromExplicitRelation->_pNextToReferenceType;
	pFromExplicitRelationNew->_pPrevToReferenceType = pFromExplicitRelation->_pPrevToReferenceType;
	pFromExplicitRelation->_pNextToReferenceType = nullptr;
	pFromExplicitRelation->_pPrevToReferenceType = nullptr;
	pFromExplicitRelation->_pToReferenceType = nullptr;
	pFromExplicitRelationNew->_pToReferenceType = this;
			
}
void  ReferenceType::DeleteAllFromExplicitRelation()
{
	 
	ExplicitRelation *elem = GetFirstFromExplicitRelation();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstFromExplicitRelation();
	}
	      
}
ExplicitRelation*  ReferenceType::GetFirstFromExplicitRelation() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstFromExplicitRelation;
	    
}
ExplicitRelation*  ReferenceType::GetLastFromExplicitRelation() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastFromExplicitRelation;
}
ExplicitRelation*  ReferenceType::GetNextFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFromExplicitRelation->_pToReferenceType == this);
	#endif
	return pFromExplicitRelation->_pNextToReferenceType; 
	      
}
ExplicitRelation*  ReferenceType::GetPrevFromExplicitRelation(ExplicitRelation*  pFromExplicitRelation) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pFromExplicitRelation == nullptr )
	{
		return _pLastFromExplicitRelation;
	}
	assert(pFromExplicitRelation->_pToReferenceType == this);
	return pFromExplicitRelation->_pPrevToReferenceType;
	      
}
unsigned long  ReferenceType::GetFromExplicitRelationCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _FromExplicitRelationCount;
	      
}
void  ReferenceType::MoveFromExplicitRelationFirst(ExplicitRelation*  pFromExplicitRelation)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelation->_pToReferenceType);
	#endif
	pFromExplicitRelation->_pToReferenceType->RemoveFromExplicitRelation(pFromExplicitRelation);
	AddFromExplicitRelationFirst(pFromExplicitRelation); 
	      
}
void  ReferenceType::MoveFromExplicitRelationLast(ExplicitRelation*  pFromExplicitRelation)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelation->_pToReferenceType);
	#endif
	pFromExplicitRelation->_pToReferenceType->RemoveFromExplicitRelation(pFromExplicitRelation);
	AddFromExplicitRelationLast(pFromExplicitRelation); 
	      
}
void  ReferenceType::MoveFromExplicitRelationAfter(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelation->_pToReferenceType);
	#endif
	pFromExplicitRelation->_pToReferenceType->RemoveFromExplicitRelation(pFromExplicitRelation);
	AddFromExplicitRelationAfter(pFromExplicitRelation,pFromExplicitRelationPos); 
}
void  ReferenceType::MoveFromExplicitRelationBefore(ExplicitRelation*  pFromExplicitRelation, ExplicitRelation*  pFromExplicitRelationPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFromExplicitRelation);
		assert(pFromExplicitRelation->_pToReferenceType);
	#endif
	pFromExplicitRelation->_pToReferenceType->RemoveFromExplicitRelation(pFromExplicitRelation);
	AddFromExplicitRelationBefore(pFromExplicitRelation,pFromExplicitRelationPos); 
}
void  ReferenceType::SortFromExplicitRelation(int (* compare)(ExplicitRelation*,ExplicitRelation*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstFromExplicitRelation(); ptr != nullptr; ptr = GetNextFromExplicitRelation(ptr))
	{
		auto pNext = GetNextFromExplicitRelation(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevFromExplicitRelation(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevFromExplicitRelation(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveFromExplicitRelationAfter(pNext, pPrev);
			}
			else
			{
				MoveFromExplicitRelationFirst(pNext);
			}
			pNext = GetNextFromExplicitRelation(ptr);
		}
	}
	      
}
void  ReferenceType::AddFomRelationChainFirst(RelationChain*  pFomRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain);
		#if !defined(TOLERANT)
			assert( pFomRelationChain->_pToReferenceType == nullptr );
		#else
			if ( pFomRelationChain->_pToReferenceType != nullptr )
			{
				assert( pFomRelationChain->_pToReferenceType == this );
				MoveFomRelationChainFirst( pFomRelationChain );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FomRelationChainCount++;
	pFomRelationChain->_pToReferenceType = this;
	if ( _pFirstFomRelationChain != nullptr )
	{
		pFomRelationChain->_pNextToReferenceType = _pFirstFomRelationChain;
		_pFirstFomRelationChain->_pPrevToReferenceType = pFomRelationChain;
		_pFirstFomRelationChain = pFomRelationChain;
	}
	else
	{
		_pFirstFomRelationChain = pFomRelationChain;
		_pLastFomRelationChain = pFomRelationChain;
	}
	      
}
void  ReferenceType::AddFomRelationChainLast(RelationChain*  pFomRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain);
		#if !defined(TOLERANT)
			assert(pFomRelationChain->_pToReferenceType == nullptr);
		#else
			if(pFomRelationChain->_pToReferenceType != nullptr)
			{
				assert(pFomRelationChain->_pToReferenceType == this);
				MoveFomRelationChainLast(pFomRelationChain);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FomRelationChainCount++;
	pFomRelationChain->_pToReferenceType = this;
	if (_pLastFomRelationChain != nullptr)
	{
		pFomRelationChain->_pPrevToReferenceType = _pLastFomRelationChain;
		_pLastFomRelationChain->_pNextToReferenceType = pFomRelationChain;
		_pLastFomRelationChain = pFomRelationChain;
	}
	else
	{
		_pLastFomRelationChain = pFomRelationChain;
		_pFirstFomRelationChain = pFomRelationChain;
	}
	      
}
void  ReferenceType::AddFomRelationChainAfter(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain);
		assert(pFomRelationChainPos);
		assert(pFomRelationChainPos->_pToReferenceType == this);
		#if !defined(TOLERANT)
			assert(pFomRelationChain->_pToReferenceType == nullptr);
		#else
			if(pFomRelationChain->_pToReferenceType != nullptr)
			{
				assert(pFomRelationChain->_pToReferenceType == this);
				MoveFomRelationChainAfter(pFomRelationChain,pFomRelationChainPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FomRelationChainCount++;
	pFomRelationChain->_pToReferenceType = this;
	pFomRelationChain->_pPrevToReferenceType = pFomRelationChainPos;
	pFomRelationChain->_pNextToReferenceType = pFomRelationChainPos->_pNextToReferenceType;
	pFomRelationChainPos->_pNextToReferenceType  = pFomRelationChain;
	if (pFomRelationChain->_pNextToReferenceType != nullptr)
	{
		pFomRelationChain->_pNextToReferenceType->_pPrevToReferenceType = pFomRelationChain;
	}
	else
	{
		_pLastFomRelationChain = pFomRelationChain;
	}
	      
}
void  ReferenceType::AddFomRelationChainBefore(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain);
		assert(pFomRelationChainPos);
		assert(pFomRelationChainPos->_pToReferenceType == this);
		#if !defined(TOLERANT)
			assert(pFomRelationChain->_pToReferenceType == nullptr);
		#else
			if (pFomRelationChain->_pToReferenceType != nullptr)
			{
				assert(pFomRelationChain->_pToReferenceType == this);
				MoveFomRelationChainBefore(pFomRelationChain,pFomRelationChainPos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FomRelationChainCount++;
	pFomRelationChain->_pToReferenceType = this;
	pFomRelationChain->_pNextToReferenceType = pFomRelationChainPos;
	pFomRelationChain->_pPrevToReferenceType = pFomRelationChainPos->_pPrevToReferenceType;
	pFomRelationChainPos->_pPrevToReferenceType = pFomRelationChain;
	if ( pFomRelationChain->_pPrevToReferenceType != nullptr )
	{
		pFomRelationChain->_pPrevToReferenceType->_pNextToReferenceType = pFomRelationChain;
	}
	else
	{
		_pFirstFomRelationChain = pFomRelationChain;
	}
			
}
void  ReferenceType::RemoveFomRelationChain(RelationChain*  pFomRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain);
		#if !defined(TOLERANT)
			assert( pFomRelationChain->_pToReferenceType == this );
		#else
			if( pFomRelationChain->_pToReferenceType != this )
			{
				assert( pFomRelationChain->_pToReferenceType == nullptr );
				if( pFomRelationChain->_pPrevToReferenceType != nullptr )
				{
					assert( pFomRelationChain->_pPrevToReferenceType->_pNextToReferenceType != pFomRelationChain );
				}
				if( pFomRelationChain->_pNextToReferenceType != nullptr )
				{
					assert( pFomRelationChain->_pNextToReferenceType->_pPrevToReferenceType != pFomRelationChain );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_FomRelationChainCount++;
	if ( pFomRelationChain->_pNextToReferenceType != nullptr )
	{
		pFomRelationChain->_pNextToReferenceType->_pPrevToReferenceType = pFomRelationChain->_pPrevToReferenceType;
	}
	else
	{
		_pLastFomRelationChain = pFomRelationChain->_pPrevToReferenceType;
	}
	if ( pFomRelationChain->_pPrevToReferenceType != nullptr )
	{
		pFomRelationChain->_pPrevToReferenceType->_pNextToReferenceType = pFomRelationChain->_pNextToReferenceType;
	}
	else
	{
		_pFirstFomRelationChain = pFomRelationChain->_pNextToReferenceType;
	}
	pFomRelationChain->_pPrevToReferenceType = nullptr;
	pFomRelationChain->_pNextToReferenceType = nullptr;
	pFomRelationChain->_pToReferenceType = nullptr;
			
}
void  ReferenceType::ReplaceFomRelationChain(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainNew)
{
	
	assert(this);
	assert(pFomRelationChain->_pToReferenceType == this);
	assert(pFomRelationChainNew->_pToReferenceType == nullptr);
	if (pFomRelationChain->_pNextToReferenceType != nullptr)
	{
		pFomRelationChain->_pNextToReferenceType->_pPrevToReferenceType = pFomRelationChainNew;
	}
	else
	{
		_pLastFomRelationChain = pFomRelationChainNew;
	}
	if (pFomRelationChain->_pPrevToReferenceType != nullptr)
	{
		pFomRelationChain->_pPrevToReferenceType->_pNextToReferenceType = pFomRelationChainNew;
	}
	else
	{
		_pFirstFomRelationChain = pFomRelationChainNew;
	}
	pFomRelationChainNew->_pNextToReferenceType = pFomRelationChain->_pNextToReferenceType;
	pFomRelationChainNew->_pPrevToReferenceType = pFomRelationChain->_pPrevToReferenceType;
	pFomRelationChain->_pNextToReferenceType = nullptr;
	pFomRelationChain->_pPrevToReferenceType = nullptr;
	pFomRelationChain->_pToReferenceType = nullptr;
	pFomRelationChainNew->_pToReferenceType = this;
			
}
void  ReferenceType::DeleteAllFomRelationChain()
{
	 
	RelationChain *elem = GetFirstFomRelationChain();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstFomRelationChain();
	}
	      
}
RelationChain*  ReferenceType::GetFirstFomRelationChain() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstFomRelationChain;
	    
}
RelationChain*  ReferenceType::GetLastFomRelationChain() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastFomRelationChain;
}
RelationChain*  ReferenceType::GetNextFomRelationChain(RelationChain*  pFomRelationChain) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pFomRelationChain->_pToReferenceType == this);
	#endif
	return pFomRelationChain->_pNextToReferenceType; 
	      
}
RelationChain*  ReferenceType::GetPrevFomRelationChain(RelationChain*  pFomRelationChain) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pFomRelationChain == nullptr )
	{
		return _pLastFomRelationChain;
	}
	assert(pFomRelationChain->_pToReferenceType == this);
	return pFomRelationChain->_pPrevToReferenceType;
	      
}
unsigned long  ReferenceType::GetFomRelationChainCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _FomRelationChainCount;
	      
}
void  ReferenceType::MoveFomRelationChainFirst(RelationChain*  pFomRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFomRelationChain);
		assert(pFomRelationChain->_pToReferenceType);
	#endif
	pFomRelationChain->_pToReferenceType->RemoveFomRelationChain(pFomRelationChain);
	AddFomRelationChainFirst(pFomRelationChain); 
	      
}
void  ReferenceType::MoveFomRelationChainLast(RelationChain*  pFomRelationChain)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFomRelationChain);
		assert(pFomRelationChain->_pToReferenceType);
	#endif
	pFomRelationChain->_pToReferenceType->RemoveFomRelationChain(pFomRelationChain);
	AddFomRelationChainLast(pFomRelationChain); 
	      
}
void  ReferenceType::MoveFomRelationChainAfter(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFomRelationChain);
		assert(pFomRelationChain->_pToReferenceType);
	#endif
	pFomRelationChain->_pToReferenceType->RemoveFomRelationChain(pFomRelationChain);
	AddFomRelationChainAfter(pFomRelationChain,pFomRelationChainPos); 
}
void  ReferenceType::MoveFomRelationChainBefore(RelationChain*  pFomRelationChain, RelationChain*  pFomRelationChainPos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pFomRelationChain);
		assert(pFomRelationChain->_pToReferenceType);
	#endif
	pFomRelationChain->_pToReferenceType->RemoveFomRelationChain(pFomRelationChain);
	AddFomRelationChainBefore(pFomRelationChain,pFomRelationChainPos); 
}
void  ReferenceType::SortFomRelationChain(int (* compare)(RelationChain*,RelationChain*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstFomRelationChain(); ptr != nullptr; ptr = GetNextFomRelationChain(ptr))
	{
		auto pNext = GetNextFomRelationChain(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevFomRelationChain(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevFomRelationChain(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveFomRelationChainAfter(pNext, pPrev);
			}
			else
			{
				MoveFomRelationChainFirst(pNext);
			}
			pNext = GetNextFomRelationChain(ptr);
		}
	}
	      
}
ReferenceType::~ReferenceType()
{
	__exit__();
}

// {user.after.class.ReferenceType.begin}
// {user.after.class.ReferenceType.end}


