// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.AttributeValue.begin}
// {user.before.class.AttributeValue.end}

void  AttributeValue::__init__(Reference*  pFromReference, AttributeDef*  pFromAttributeDef)
{
		pFromReference->AddToAttributeValueLast(this);
		pFromAttributeDef->AddToAttributeValueLast(this);
}
void  AttributeValue::__exit__()
{
		_pFromReference->RemoveToAttributeValue(this);
		_pFromAttributeDef->RemoveToAttributeValue(this);
}
Reference*  AttributeValue::GetFromReference()
{
	
	return _pFromReference;
	      
}
AttributeDef*  AttributeValue::GetFromAttributeDef()
{
	
	return _pFromAttributeDef;
	      
}
AttributeValue::~AttributeValue()
{
	__exit__();
}

// {user.after.class.AttributeValue.begin}
// {user.after.class.AttributeValue.end}


