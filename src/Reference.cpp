// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.Reference.begin}
// {user.before.class.Reference.end}

void  Reference::__init__(ReferenceType*  pFromReferenceType)
{
		pFromReferenceType->AddToReferenceLast(this);
}
void  Reference::__exit__()
{
		_pFromReferenceType->RemoveToReference(this);
		DeleteAllToAttributeValue();
}
Reference::Reference(std::string  key, ReferenceType*  pFromReferenceType)
:	_pFirstToAttributeValue(nullptr)
,	_pLastToAttributeValue(nullptr)
,	_ToAttributeValueCount(0UL)
,	_key(key)
,	_pNextFromReferenceType(nullptr)
,	_pPrevFromReferenceType(nullptr)
,	_pFromReferenceType(pFromReferenceType)
{
		__init__(pFromReferenceType);
}
ReferenceType*  Reference::GetFromReferenceType()
{
	
	return _pFromReferenceType;
	      
}
const std::string&  Reference::key()
{
	return _key;
}
void  Reference::AddToAttributeValueFirst(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert( pToAttributeValue->_pFromReference == nullptr );
		#else
			if ( pToAttributeValue->_pFromReference != nullptr )
			{
				assert( pToAttributeValue->_pFromReference == this );
				MoveToAttributeValueFirst( pToAttributeValue );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromReference = this;
	if ( _pFirstToAttributeValue != nullptr )
	{
		pToAttributeValue->_pNextFromReference = _pFirstToAttributeValue;
		_pFirstToAttributeValue->_pPrevFromReference = pToAttributeValue;
		_pFirstToAttributeValue = pToAttributeValue;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue;
		_pLastToAttributeValue = pToAttributeValue;
	}
	      
}
void  Reference::AddToAttributeValueLast(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromReference == nullptr);
		#else
			if(pToAttributeValue->_pFromReference != nullptr)
			{
				assert(pToAttributeValue->_pFromReference == this);
				MoveToAttributeValueLast(pToAttributeValue);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromReference = this;
	if (_pLastToAttributeValue != nullptr)
	{
		pToAttributeValue->_pPrevFromReference = _pLastToAttributeValue;
		_pLastToAttributeValue->_pNextFromReference = pToAttributeValue;
		_pLastToAttributeValue = pToAttributeValue;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue;
		_pFirstToAttributeValue = pToAttributeValue;
	}
	      
}
void  Reference::AddToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		assert(pToAttributeValuePos);
		assert(pToAttributeValuePos->_pFromReference == this);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromReference == nullptr);
		#else
			if(pToAttributeValue->_pFromReference != nullptr)
			{
				assert(pToAttributeValue->_pFromReference == this);
				MoveToAttributeValueAfter(pToAttributeValue,pToAttributeValuePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromReference = this;
	pToAttributeValue->_pPrevFromReference = pToAttributeValuePos;
	pToAttributeValue->_pNextFromReference = pToAttributeValuePos->_pNextFromReference;
	pToAttributeValuePos->_pNextFromReference  = pToAttributeValue;
	if (pToAttributeValue->_pNextFromReference != nullptr)
	{
		pToAttributeValue->_pNextFromReference->_pPrevFromReference = pToAttributeValue;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue;
	}
	      
}
void  Reference::AddToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		assert(pToAttributeValuePos);
		assert(pToAttributeValuePos->_pFromReference == this);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromReference == nullptr);
		#else
			if (pToAttributeValue->_pFromReference != nullptr)
			{
				assert(pToAttributeValue->_pFromReference == this);
				MoveToAttributeValueBefore(pToAttributeValue,pToAttributeValuePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromReference = this;
	pToAttributeValue->_pNextFromReference = pToAttributeValuePos;
	pToAttributeValue->_pPrevFromReference = pToAttributeValuePos->_pPrevFromReference;
	pToAttributeValuePos->_pPrevFromReference = pToAttributeValue;
	if ( pToAttributeValue->_pPrevFromReference != nullptr )
	{
		pToAttributeValue->_pPrevFromReference->_pNextFromReference = pToAttributeValue;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue;
	}
			
}
void  Reference::RemoveToAttributeValue(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert( pToAttributeValue->_pFromReference == this );
		#else
			if( pToAttributeValue->_pFromReference != this )
			{
				assert( pToAttributeValue->_pFromReference == nullptr );
				if( pToAttributeValue->_pPrevFromReference != nullptr )
				{
					assert( pToAttributeValue->_pPrevFromReference->_pNextFromReference != pToAttributeValue );
				}
				if( pToAttributeValue->_pNextFromReference != nullptr )
				{
					assert( pToAttributeValue->_pNextFromReference->_pPrevFromReference != pToAttributeValue );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	if ( pToAttributeValue->_pNextFromReference != nullptr )
	{
		pToAttributeValue->_pNextFromReference->_pPrevFromReference = pToAttributeValue->_pPrevFromReference;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue->_pPrevFromReference;
	}
	if ( pToAttributeValue->_pPrevFromReference != nullptr )
	{
		pToAttributeValue->_pPrevFromReference->_pNextFromReference = pToAttributeValue->_pNextFromReference;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue->_pNextFromReference;
	}
	pToAttributeValue->_pPrevFromReference = nullptr;
	pToAttributeValue->_pNextFromReference = nullptr;
	pToAttributeValue->_pFromReference = nullptr;
			
}
void  Reference::ReplaceToAttributeValue(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValueNew)
{
	
	assert(this);
	assert(pToAttributeValue->_pFromReference == this);
	assert(pToAttributeValueNew->_pFromReference == nullptr);
	if (pToAttributeValue->_pNextFromReference != nullptr)
	{
		pToAttributeValue->_pNextFromReference->_pPrevFromReference = pToAttributeValueNew;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValueNew;
	}
	if (pToAttributeValue->_pPrevFromReference != nullptr)
	{
		pToAttributeValue->_pPrevFromReference->_pNextFromReference = pToAttributeValueNew;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValueNew;
	}
	pToAttributeValueNew->_pNextFromReference = pToAttributeValue->_pNextFromReference;
	pToAttributeValueNew->_pPrevFromReference = pToAttributeValue->_pPrevFromReference;
	pToAttributeValue->_pNextFromReference = nullptr;
	pToAttributeValue->_pPrevFromReference = nullptr;
	pToAttributeValue->_pFromReference = nullptr;
	pToAttributeValueNew->_pFromReference = this;
			
}
void  Reference::DeleteAllToAttributeValue()
{
	 
	AttributeValue *elem = GetFirstToAttributeValue();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstToAttributeValue();
	}
	      
}
AttributeValue*  Reference::GetFirstToAttributeValue() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstToAttributeValue;
	    
}
AttributeValue*  Reference::GetLastToAttributeValue() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastToAttributeValue;
}
AttributeValue*  Reference::GetNextToAttributeValue(AttributeValue*  pToAttributeValue) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue->_pFromReference == this);
	#endif
	return pToAttributeValue->_pNextFromReference; 
	      
}
AttributeValue*  Reference::GetPrevToAttributeValue(AttributeValue*  pToAttributeValue) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pToAttributeValue == nullptr )
	{
		return _pLastToAttributeValue;
	}
	assert(pToAttributeValue->_pFromReference == this);
	return pToAttributeValue->_pPrevFromReference;
	      
}
unsigned long  Reference::GetToAttributeValueCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _ToAttributeValueCount;
	      
}
void  Reference::MoveToAttributeValueFirst(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromReference);
	#endif
	pToAttributeValue->_pFromReference->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueFirst(pToAttributeValue); 
	      
}
void  Reference::MoveToAttributeValueLast(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromReference);
	#endif
	pToAttributeValue->_pFromReference->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueLast(pToAttributeValue); 
	      
}
void  Reference::MoveToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromReference);
	#endif
	pToAttributeValue->_pFromReference->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueAfter(pToAttributeValue,pToAttributeValuePos); 
}
void  Reference::MoveToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromReference);
	#endif
	pToAttributeValue->_pFromReference->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueBefore(pToAttributeValue,pToAttributeValuePos); 
}
void  Reference::SortToAttributeValue(int (* compare)(AttributeValue*,AttributeValue*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstToAttributeValue(); ptr != nullptr; ptr = GetNextToAttributeValue(ptr))
	{
		auto pNext = GetNextToAttributeValue(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevToAttributeValue(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevToAttributeValue(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveToAttributeValueAfter(pNext, pPrev);
			}
			else
			{
				MoveToAttributeValueFirst(pNext);
			}
			pNext = GetNextToAttributeValue(ptr);
		}
	}
	      
}
Reference::~Reference()
{
	__exit__();
}

// {user.after.class.Reference.begin}
// {user.after.class.Reference.end}


