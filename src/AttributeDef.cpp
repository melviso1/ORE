// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.AttributeDef.begin}
// {user.before.class.AttributeDef.end}

void  AttributeDef::__init__(ReferenceType*  pFromReferenceType)
{
		pFromReferenceType->AddToAttributeDefLast(this);
}
void  AttributeDef::__exit__()
{
		_pFromReferenceType->RemoveToAttributeDef(this);
		DeleteAllToAttributeValue();
}
AttributeDef::AttributeDef(ReferenceType*  pFromReferenceType)
:	_pFirstToAttributeValue(nullptr)
,	_pLastToAttributeValue(nullptr)
,	_ToAttributeValueCount(0UL)
,	_pNextFromReferenceType(nullptr)
,	_pPrevFromReferenceType(nullptr)
,	_pFromReferenceType(pFromReferenceType)
{
		__init__(pFromReferenceType);
}
/**
* This method checks if the instance is specialized as class ExplicitRelation
**/
const bool  AttributeDef::is_ExplicitRelation() const
{
		return ( dynamic_cast<const ExplicitRelation*>(this) != nullptr );
}
ReferenceType*  AttributeDef::GetFromReferenceType()
{
	
	return _pFromReferenceType;
	      
}
void  AttributeDef::AddToAttributeValueFirst(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert( pToAttributeValue->_pFromAttributeDef == nullptr );
		#else
			if ( pToAttributeValue->_pFromAttributeDef != nullptr )
			{
				assert( pToAttributeValue->_pFromAttributeDef == this );
				MoveToAttributeValueFirst( pToAttributeValue );
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromAttributeDef = this;
	if ( _pFirstToAttributeValue != nullptr )
	{
		pToAttributeValue->_pNextFromAttributeDef = _pFirstToAttributeValue;
		_pFirstToAttributeValue->_pPrevFromAttributeDef = pToAttributeValue;
		_pFirstToAttributeValue = pToAttributeValue;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue;
		_pLastToAttributeValue = pToAttributeValue;
	}
	      
}
void  AttributeDef::AddToAttributeValueLast(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromAttributeDef == nullptr);
		#else
			if(pToAttributeValue->_pFromAttributeDef != nullptr)
			{
				assert(pToAttributeValue->_pFromAttributeDef == this);
				MoveToAttributeValueLast(pToAttributeValue);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromAttributeDef = this;
	if (_pLastToAttributeValue != nullptr)
	{
		pToAttributeValue->_pPrevFromAttributeDef = _pLastToAttributeValue;
		_pLastToAttributeValue->_pNextFromAttributeDef = pToAttributeValue;
		_pLastToAttributeValue = pToAttributeValue;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue;
		_pFirstToAttributeValue = pToAttributeValue;
	}
	      
}
void  AttributeDef::AddToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		assert(pToAttributeValuePos);
		assert(pToAttributeValuePos->_pFromAttributeDef == this);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromAttributeDef == nullptr);
		#else
			if(pToAttributeValue->_pFromAttributeDef != nullptr)
			{
				assert(pToAttributeValue->_pFromAttributeDef == this);
				MoveToAttributeValueAfter(pToAttributeValue,pToAttributeValuePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromAttributeDef = this;
	pToAttributeValue->_pPrevFromAttributeDef = pToAttributeValuePos;
	pToAttributeValue->_pNextFromAttributeDef = pToAttributeValuePos->_pNextFromAttributeDef;
	pToAttributeValuePos->_pNextFromAttributeDef  = pToAttributeValue;
	if (pToAttributeValue->_pNextFromAttributeDef != nullptr)
	{
		pToAttributeValue->_pNextFromAttributeDef->_pPrevFromAttributeDef = pToAttributeValue;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue;
	}
	      
}
void  AttributeDef::AddToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		assert(pToAttributeValuePos);
		assert(pToAttributeValuePos->_pFromAttributeDef == this);
		#if !defined(TOLERANT)
			assert(pToAttributeValue->_pFromAttributeDef == nullptr);
		#else
			if (pToAttributeValue->_pFromAttributeDef != nullptr)
			{
				assert(pToAttributeValue->_pFromAttributeDef == this);
				MoveToAttributeValueBefore(pToAttributeValue,pToAttributeValuePos);
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	pToAttributeValue->_pFromAttributeDef = this;
	pToAttributeValue->_pNextFromAttributeDef = pToAttributeValuePos;
	pToAttributeValue->_pPrevFromAttributeDef = pToAttributeValuePos->_pPrevFromAttributeDef;
	pToAttributeValuePos->_pPrevFromAttributeDef = pToAttributeValue;
	if ( pToAttributeValue->_pPrevFromAttributeDef != nullptr )
	{
		pToAttributeValue->_pPrevFromAttributeDef->_pNextFromAttributeDef = pToAttributeValue;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue;
	}
			
}
void  AttributeDef::RemoveToAttributeValue(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue);
		#if !defined(TOLERANT)
			assert( pToAttributeValue->_pFromAttributeDef == this );
		#else
			if( pToAttributeValue->_pFromAttributeDef != this )
			{
				assert( pToAttributeValue->_pFromAttributeDef == nullptr );
				if( pToAttributeValue->_pPrevFromAttributeDef != nullptr )
				{
					assert( pToAttributeValue->_pPrevFromAttributeDef->_pNextFromAttributeDef != pToAttributeValue );
				}
				if( pToAttributeValue->_pNextFromAttributeDef != nullptr )
				{
					assert( pToAttributeValue->_pNextFromAttributeDef->_pPrevFromAttributeDef != pToAttributeValue );
				}
				return;
			}
		#endif //TOLERANT
	#endif //OPTIMISTIC
	_ToAttributeValueCount++;
	if ( pToAttributeValue->_pNextFromAttributeDef != nullptr )
	{
		pToAttributeValue->_pNextFromAttributeDef->_pPrevFromAttributeDef = pToAttributeValue->_pPrevFromAttributeDef;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValue->_pPrevFromAttributeDef;
	}
	if ( pToAttributeValue->_pPrevFromAttributeDef != nullptr )
	{
		pToAttributeValue->_pPrevFromAttributeDef->_pNextFromAttributeDef = pToAttributeValue->_pNextFromAttributeDef;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValue->_pNextFromAttributeDef;
	}
	pToAttributeValue->_pPrevFromAttributeDef = nullptr;
	pToAttributeValue->_pNextFromAttributeDef = nullptr;
	pToAttributeValue->_pFromAttributeDef = nullptr;
			
}
void  AttributeDef::ReplaceToAttributeValue(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValueNew)
{
	
	assert(this);
	assert(pToAttributeValue->_pFromAttributeDef == this);
	assert(pToAttributeValueNew->_pFromAttributeDef == nullptr);
	if (pToAttributeValue->_pNextFromAttributeDef != nullptr)
	{
		pToAttributeValue->_pNextFromAttributeDef->_pPrevFromAttributeDef = pToAttributeValueNew;
	}
	else
	{
		_pLastToAttributeValue = pToAttributeValueNew;
	}
	if (pToAttributeValue->_pPrevFromAttributeDef != nullptr)
	{
		pToAttributeValue->_pPrevFromAttributeDef->_pNextFromAttributeDef = pToAttributeValueNew;
	}
	else
	{
		_pFirstToAttributeValue = pToAttributeValueNew;
	}
	pToAttributeValueNew->_pNextFromAttributeDef = pToAttributeValue->_pNextFromAttributeDef;
	pToAttributeValueNew->_pPrevFromAttributeDef = pToAttributeValue->_pPrevFromAttributeDef;
	pToAttributeValue->_pNextFromAttributeDef = nullptr;
	pToAttributeValue->_pPrevFromAttributeDef = nullptr;
	pToAttributeValue->_pFromAttributeDef = nullptr;
	pToAttributeValueNew->_pFromAttributeDef = this;
			
}
void  AttributeDef::DeleteAllToAttributeValue()
{
	 
	AttributeValue *elem = GetFirstToAttributeValue();
	while( elem != nullptr )
	{
		delete elem;
		elem = GetFirstToAttributeValue();
	}
	      
}
AttributeValue*  AttributeDef::GetFirstToAttributeValue() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pFirstToAttributeValue;
	    
}
AttributeValue*  AttributeDef::GetLastToAttributeValue() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif //OPTIMISTIC
	return _pLastToAttributeValue;
}
AttributeValue*  AttributeDef::GetNextToAttributeValue(AttributeValue*  pToAttributeValue) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
		assert(pToAttributeValue->_pFromAttributeDef == this);
	#endif
	return pToAttributeValue->_pNextFromAttributeDef; 
	      
}
AttributeValue*  AttributeDef::GetPrevToAttributeValue(AttributeValue*  pToAttributeValue) const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	if (pToAttributeValue == nullptr )
	{
		return _pLastToAttributeValue;
	}
	assert(pToAttributeValue->_pFromAttributeDef == this);
	return pToAttributeValue->_pPrevFromAttributeDef;
	      
}
unsigned long  AttributeDef::GetToAttributeValueCount() const
{
	
	#if !defined(OPTIMISTIC)
		assert(this);
	#endif
	return _ToAttributeValueCount;
	      
}
void  AttributeDef::MoveToAttributeValueFirst(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromAttributeDef);
	#endif
	pToAttributeValue->_pFromAttributeDef->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueFirst(pToAttributeValue); 
	      
}
void  AttributeDef::MoveToAttributeValueLast(AttributeValue*  pToAttributeValue)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromAttributeDef);
	#endif
	pToAttributeValue->_pFromAttributeDef->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueLast(pToAttributeValue); 
	      
}
void  AttributeDef::MoveToAttributeValueAfter(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromAttributeDef);
	#endif
	pToAttributeValue->_pFromAttributeDef->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueAfter(pToAttributeValue,pToAttributeValuePos); 
}
void  AttributeDef::MoveToAttributeValueBefore(AttributeValue*  pToAttributeValue, AttributeValue*  pToAttributeValuePos)
{
	
	#if !defined(OPTIMISTIC)
		assert(pToAttributeValue);
		assert(pToAttributeValue->_pFromAttributeDef);
	#endif
	pToAttributeValue->_pFromAttributeDef->RemoveToAttributeValue(pToAttributeValue);
	AddToAttributeValueBefore(pToAttributeValue,pToAttributeValuePos); 
}
void  AttributeDef::SortToAttributeValue(int (* compare)(AttributeValue*,AttributeValue*))
{
	
	/**
		This algorithm is a kind of bubble sort. Dont expect high performance!
	**/
	for(auto ptr=GetFirstToAttributeValue(); ptr != nullptr; ptr = GetNextToAttributeValue(ptr))
	{
		auto pNext = GetNextToAttributeValue(ptr);
		while (pNext !=nullptr &&  compare(ptr, pNext) > 0)
		{
			auto pPrev = GetPrevToAttributeValue(ptr);
			while (pPrev != nullptr &&  compare(pPrev, pNext) > 0)
			{
				pPrev = GetPrevToAttributeValue(pPrev);
			}
			if (pPrev != nullptr)
			{
				MoveToAttributeValueAfter(pNext, pPrev);
			}
			else
			{
				MoveToAttributeValueFirst(pNext);
			}
			pNext = GetNextToAttributeValue(ptr);
		}
	}
	      
}
AttributeDef::~AttributeDef()
{
	__exit__();
}

// {user.after.class.AttributeDef.begin}
// {user.after.class.AttributeDef.end}


