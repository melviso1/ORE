// {user.before.include.begin}
// {user.before.include.end}

#include "ORE.h"

#include <assert.h>

// {user.before.class.RelationChain.begin}
// {user.before.class.RelationChain.end}

void  RelationChain::__init__(ReferenceType*  pFromReferenceType, ReferenceType*  pToReferenceType)
{
		pFromReferenceType->AddToRelationChainLast(this);
		pToReferenceType->AddFomRelationChainLast(this);
}
void  RelationChain::__exit__()
{
		_pFromReferenceType->RemoveToRelationChain(this);
		_pToReferenceType->RemoveFomRelationChain(this);
}
/**
* Las cadenas de relaciones no pueden ser creadas libremente. Son resultado de autodiscover
**/
RelationChain::RelationChain(ExplicitRelation*  pExplicitRelation, RelationChain*  prev)
:	_relations( )
,	_pNextFromReferenceType(nullptr)
,	_pPrevFromReferenceType(nullptr)
,	_pFromReferenceType(pExplicitRelation->GetFromReferenceType())
,	_pNextToReferenceType(nullptr)
,	_pPrevToReferenceType(nullptr)
,	_pToReferenceType(pExplicitRelation->GetToReferenceType())
{
		if (prev != nullptr )
		{
			_relations = prev->_relations;
			_pFromReferenceType = prev->GetFromReferenceType();
		}
		__init__(_pFromReferenceType,_pToReferenceType);
		_relations.push_back(pExplicitRelation);	
		Populate();
}
ReferenceType*  RelationChain::GetFromReferenceType()
{
	
	return _pFromReferenceType;
	      
}
ReferenceType*  RelationChain::GetToReferenceType()
{
	
	return _pToReferenceType;
	      
}
/**
* La mision de este metodo es la de generar todas las cadenas de propagacion de relaciones
**/
void  RelationChain::Populate()
{
	// iterate through relations
	
	auto ptr = GetToReferenceType()->GetFirstToAttributeDef();
	while( ptr != nullptr )
	{
		ExplicitRelation *pExplicitRelation = dynamic_cast<ExplicitRelation*>(ptr);
		if ( ptr != nullptr )
		{
			//If we take account of this relation we skip it
			auto it = std::find(begin(_relations), end(_relations), ptr);
			if (it != end(_relations) )
			{
				new RelationChain(pExplicitRelation, this);
			}
		}
		ptr = GetToReferenceType()->GetNextToAttributeDef(ptr);
	}
}
RelationChain::~RelationChain()
{
		__exit__();
}

// {user.after.class.RelationChain.begin}
// {user.after.class.RelationChain.end}


