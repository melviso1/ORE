# definitions

CC = g++
AR = ar
CFLAGS = -ggdb -fexceptions -Wall -std=c++0x -fPIC
INCLUDE = -Iinclude
SRC = src

all : ORE.so

ORE.so: ReferenceType.o Reference.o AttributeDef.o RelationChain.o AttributeValue.o ExplicitRelation.o
	$(CC) -shared  ReferenceType.o Reference.o AttributeDef.o RelationChain.o AttributeValue.o ExplicitRelation.o  -o ORE.so

ReferenceType.o: src/ReferenceType.cpp include/ReferenceType.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/ReferenceType.cpp -o ReferenceType.o

Reference.o: src/Reference.cpp include/Reference.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/Reference.cpp -o Reference.o

AttributeDef.o: src/AttributeDef.cpp include/AttributeDef.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/AttributeDef.cpp -o AttributeDef.o

RelationChain.o: src/RelationChain.cpp include/RelationChain.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/RelationChain.cpp -o RelationChain.o

AttributeValue.o: src/AttributeValue.cpp include/AttributeValue.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/AttributeValue.cpp -o AttributeValue.o

ExplicitRelation.o: src/ExplicitRelation.cpp include/ExplicitRelation.h
	$(CC) $(CFLAGS) $(INCLUDE) -c src/ExplicitRelation.cpp -o ExplicitRelation.o

clean:
	rm *.o ORE.so

